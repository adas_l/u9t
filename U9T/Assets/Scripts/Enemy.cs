﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Renderer))]
public class Enemy : MonoBehaviour
{
    //settings for Enemies (could be stored somewhere else and be setable in insepctor for level designer)
    [SerializeField] private float maxSpeed = 30f; //it's degrees of rotation per second
    [SerializeField] private float acceleration = 0.77f;
    [SerializeField] private float MAX_HEIGHT = 6.6f;
    [SerializeField] private float MIN_HEIGHT = 2.2f;
    [SerializeField] private float MAX_HEALTH = 4.5f; //how many seconds of player focus it can withstand
    [SerializeField] private LineRenderer laserTemplate;

    [SerializeField] private float initializationDelay = 0f;

    private LineRenderer laser;


    //that event system sucks for this tiny purpose
    bool isFocused = false;
    public void OnFocusStart() {
        Debug.Log("START FOCUS");
        isFocused = true;
    }
    public void OnFocusEnd() {
        Debug.Log("FOCUS ENDED");
        isFocused = false;
    }
    public void OnFocusClick() {
        Debug.Log(">>> FOCUS POKUS <<<");
    }


    public void EndureFocus() {
        if(focusTime > 0) {
            health -= Time.deltaTime;
        }
        if(health < 0) {
            Deactivate();
        }
        focusTime += Time.deltaTime;
    }



    private enum EnemyAction {
        MULTIPLY, //multiply when it is trying to rise and split into two pieces
        RUN,  //just flying around and resting - default state
        FIRE,  //firing at player
        STUCK }  //if being focused or colided with other enemy

    //state of enemy (excluding transform)
    private EnemyAction currentRoutine = EnemyAction.STUCK;
    private float currentSpeed = 0f;
    private float health = 0f;
    private float focusTime = 0f;
    private float flyingTime = 0f;

    

    // Start is called before the first frame update
    void Awake() {
        //small randomization  so they do not look boring and synced like one piece
        maxSpeed *= Random.Range(0.9f, 1.1f); 
        flyingTime = Random.Range(0f, 1.2f);

        health = MAX_HEALTH;
        //StartCoroutine(FlyingAround()); //it's still so ugly that you need to pass string?!
        Deactivate();
        Invoke("Reactivate", initializationDelay);
    }

    // Update is called once per frame
    void Update()
    {
        if(isFocused) {
            focusTime += Time.deltaTime;
            EndureFocus(); //this can be single frame out of sync if events are processed after updates, but that doesn't really matter
        } else {
            focusTime = Mathf.Max(0, focusTime - 2 * Time.deltaTime);
        }

        if(flyingTime > 6.6f) {
            flyingTime = 0;
            StartCoroutine(Shooting());
        }
    }
    
    IEnumerator FlyingAround() {
        Debug.Log("Starting to fly around");
        if(currentRoutine == EnemyAction.RUN) {
            Debug.LogWarning("do not launch two routines at once");
            yield break;
        }
        currentRoutine = EnemyAction.RUN;
        while(currentRoutine == EnemyAction.RUN) {
            //TODO: make it move up/down closer further so it isn't so boring
            currentSpeed += acceleration;
            currentSpeed *= 1 - Mathf.Clamp(currentSpeed, 0, focusTime / MAX_HEALTH); //if half health is focused then has no speed
            currentSpeed = Mathf.Clamp(currentSpeed, 0, maxSpeed);
            transform.RotateAround(Vector3.zero, Vector3.up, currentSpeed * Time.deltaTime);
            flyingTime += Time.deltaTime;
            yield return null;
        }
        while(currentSpeed > 0) {
            currentSpeed = Mathf.Max(0, currentSpeed - acceleration * 3);
            transform.RotateAround(Vector3.zero, Vector3.up, currentSpeed * Time.deltaTime);
            yield return null;
        }
    }

    IEnumerator Shooting() {     
        if(currentRoutine == EnemyAction.FIRE) {
            Debug.LogWarning("you shall not start shooting routine twice");
            yield break;
        }
        currentRoutine = EnemyAction.FIRE;
        var material = GetComponent<Renderer>().material;
        var initialColor = material.color;
        var shootColor = Color.red; //because no time - could use some fancy animation
        var initialSize = transform.localScale;
        var shootSize = new Vector3(0.9f * initialSize.x, 0.9f * initialSize.y, 0.9f * initialSize.z);

        var timer = 0f;
        var duration = 2.2f;
        while(timer < duration && currentRoutine == EnemyAction.FIRE) {
            if(isFocused) {
                timer += Time.deltaTime * 0.1f; //should go down, but no time to make it work
            } else {
                timer += Time.deltaTime;
            }
            var phase = Mathf.Min(1, timer / duration);
            material.color = Color.Lerp(initialColor, shootColor, phase);
            transform.localScale = Vector3.Lerp(initialSize, shootSize, phase);
            yield return null;
        }
        laser = GameObject.Instantiate(laserTemplate);
        laser.SetPosition(0, transform.position);
        var target = FindObjectOfType<Player>();
        target.GetHit();
        laser.SetPosition(1, target.transform.position); //player is at center of the world
        while(timer > 0) {
            timer -= Time.deltaTime * 7;
            var phase = Mathf.Max(0, timer / duration);
            material.color = Color.Lerp(initialColor, shootColor, phase);
            transform.localScale = Vector3.Lerp(initialSize, shootSize, phase);
            yield return null;
        }
        Destroy(laser.gameObject);            
        StartCoroutine(FlyingAround()); //it's not nice that this routine sets what's after it... shoud be handled in update
    }


    private void Deactivate() {
        if(laser != null) {
            Destroy(laser.gameObject);
        }
        //TODO: nice flash that it dissaperared
        gameObject.SetActive(false);
        GetComponent<Renderer>().enabled = false; //yeah, I know should not use getComponent often
        StopAllCoroutines();
        currentRoutine = EnemyAction.STUCK;
        Invoke("Reactivate", 11f);
    }

    private void Reactivate() {
        Debug.Log("Reacitvation...");
        //TODO: nice flash that it was made
        gameObject.SetActive(true);
        GetComponent<Renderer>().enabled = true; //yeah, I know should not use getComponent often
        StartCoroutine(FlyingAround());
    }

}

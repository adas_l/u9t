﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private int deaths = 0;
    [SerializeField] UIMessageBox badNeon;
    [SerializeField] UIMessageBox goodNeon;

    public void GetHit() {
        switch(deaths) {
        case 0:
            badNeon.Display("You DIED!");
            break;
        case 1:
            badNeon.Display("You died AGAIN!");
            break;
        case 2:
            badNeon.Display("You died 3rd time!!!");
            break;
        default:
            badNeon.Display("Why don't you want to die?!");
            break;
        }
        deaths++;
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
}

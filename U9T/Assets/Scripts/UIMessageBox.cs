﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UIMessageBox : MonoBehaviour
{
    private Text label;
    
    void Awake() {
        label = GetComponent<Text>();
        label.text = "";
    }

    public void Display(string message) {
        label.text = message;
        Invoke("Clear", 0.3f); //should use some fancy animation
    }

    private void Clear() {
        label.text = "";
    }
}
